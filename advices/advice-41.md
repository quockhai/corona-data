##### Put on, use, take off add dispose of a masks
***

* Before putting on a mask, clean hands with alcohol-based hand rub or soap and water.
* Cover mouth and nose with mask and make sure there are no gaps between your face and the mask.
* Avoid touching the mask while using it; if you do, clean your hands with alcohol-based hand rub or soap and water.
* Replace the mask with a new one as soon as it is damp and do not re-use single-use masks.
* To remove the mask: remove it from behind (do not touch the front of mask); discard immediately in a closed bin; clean hands with alcohol-based hand rub or soap and water.