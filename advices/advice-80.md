##### Corona symptoms
***

**Common symptoms:**

* Headache and malaise
* Runny nose
* Cough or sore throat
* Muscle pain

**Potential complications:**

* High fever (above 38°C or 100.4°F)
* Trouble breathing
* Pneumonia
* Sepsis or even death