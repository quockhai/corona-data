##### Practice food safety
***

* Use different chopping boards and knives for raw meat and cooked foods
* Wash your hands between handling raw and cooked food.
* Sick animals and animals that have died of diseases should not be eaten
* Even in areas experiencing outbreaks, meat products can be safely consumed if these items are cooked thoroughly and properly handled during food preparation.