##### When to use a mask
***

* If you are healthy, you only need to wear a mask if you are taking care of a person with suspected 2019-nCoV infection.
* Wear a mask if you are coughing or sneezing.
* Masks are effective only when used in combination with frequent hand-cleaning with alcohol-based hand rub or soap and water.
* If you wear a mask, then you must know how to use it and dispose of it properly.