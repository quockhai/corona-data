##### Shopping/Working in wet markets in China and Southeast Asia
***

**Shopping in wet markets in China and Southeast Asia? Stay healthy!**

* Wash hands with soap and water after touching animals and animal products
* Avoid touching eyes, nose and mouth
* Avoid contact with sick animals and spoiled meat
* Avoid contact with stray animals, waste and fluids in market

**Working in wet markets in China and Southeast Asia? Stay healthy!**

* Wear protective gowns, gloves and facial protection while handling animals and animal products
* Remove protective clothing after work, wash daily and leave at the work site
* Avoid exposing family members to soiled work clothing and shoes
* Frequently wash your hands with soap and water after touching animals and animal products
* Disinfect equipment and working area at least once a day