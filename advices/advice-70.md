##### Stay healthy while travelling
***

* Avoid travel if you have a fever and cough
* If you have a fever, cough and difficulty breathing seek medical care early and share previous travel history with your health care provider
* Avoid close contact with people suffering from a fever and cough
* Frequently clean hands by using alcohol-based hand rub or soap and water
* Avoid touching eyes, nose or mouth
* When coughing and sneezing cover mouth and nose with flexed elbow or tissue - throw tissue away immediately and wash hands
* If you choose to wear a face mask, be sure to cover mouth and nose - avoid touching mask once it's on 
* Immediately discard single-use mask after each use and wash hands after removing masks
* If you become sick while travelling, inform crew and seek medical care early
* If you seek medical attention, share travel history with your health care provider
* Eat only welI-cooked food
* Avoid spitting in public
* Avoid close contact and travel with animals that are sick

