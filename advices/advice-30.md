##### Protect yourself and others from getting sick
***

**Wash your hands:**

* Wash your hands with soap and running water when hands are visibly dirty
* If your hands are not visibly dirty, frequently clean them by using alcohol-based hand rub or soap and water


**Wash your hands:**

* after coughing or sneezing
* when caring for the sick
* before, during and after you prepare food
* before eating
* after toilet use
* when hands are visibly dirty
* after handling animals or animal waste


**Protect others from getting sick:**

* When coughing and sneezing cover mouth and nose with flexed elbow or tissue
* Throw tissue into closed bin immediately after use
* Clean hands with alcohol-based hand rub or soap and water after coughing or sneezing and when caring for the sick
* Avoid close contact when you are experiencing cough and fever
* Avoid spitting in public
* If you have fever, cough and difficulty breathing seek medical care early and share previous travel history with your health care provider