##### Can eating garlic help prevent infection with the new coronavirus?
***

Garlic is a healthy food that may have some antimicrobial properties. However, there is no evidence from the current outbreak that eating garlic has protected people from the new coronavirus.