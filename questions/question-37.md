##### Can regularly rinsing your nose with saline help prevent infection with the new coronavirus?
***

No. There is no evidence that regularly rinsing the nose with saline has protected people from infection with the new coronavirus. 

There is some limited evidence that regularly rinsing nose with saline can help people recover more quickly from the common cold. However, regularly rinsing the nose has not been shown to prevent respiratory infections.