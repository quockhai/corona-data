##### Drinking alcohol does not protect you against COVID-19 and can be dangerous
***

Frequent or excessive alcohol consumption can increase your risk of health problems. 