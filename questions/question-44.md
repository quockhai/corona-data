##### Is wearing rubber gloves while out in public effective in preventing the new Coronavirus infection?
***

No. Regularly washing your bare hands offers more protection against catching COVID-19 than wearing rubber gloves.

You can still pick up COVID-19 contamination on rubber gloves. If you then touch your face, the contamination goes from your glove to your face and can infect you.