##### Can an ultraviolet disinfection lamp kill the new coronavirus?
***

UV lamps should not be used to sterilize hands or other areas of skin as UV radiation can cause skin irritation.