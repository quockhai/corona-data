##### Should I avoid shaking hands because of the new coronavirus?
***

Yes. Respiratory viruses can be passed by shaking hands and touching your eyes, nose and mouth.

Greet people with a wave, a nod or a bow instead.