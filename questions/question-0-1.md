##### What is a coronavirus?
***

Coronaviruses are a large family of viruses that are known to cause illness ranging from the common cold to more severe diseases such as Middle East Respiratory Syndrome (MERS) and Severe Acute Respiratory Syndrome (SARS). 

[**WHO: Coronavirus - questions and answers (Q&A)**](https://www.youtube.com/watch?v=OZcRD9fV7jo&feature=emb_title)