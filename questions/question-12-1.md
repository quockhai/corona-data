##### Are antibiotics effective in preventing and treating the new coronavirus?
***

No, antibiotics do not work against viruses, only bacteria.

The new coronavirus (2019-nCoV) is a virus and, therefore, antibiotics should not be used as a means of prevention or treatment.

However, if you are hospitalized for the 2019-nCoV, you may receive antibiotics because bacterial co-infection is possible.