##### Are there any specific medicines to prevent or treat the new coronavirus?
***

To date, there is no specific medicine recommended to prevent or treat the new coronavirus.

However, those infected with the virus should receive appropriate care to relieve and treat symptoms, and those with severe illness should receive optimized supportive care. Some specific treatments are under investigation, and will be tested through clinical trials. WHO is helping to accelerate research and development efforts with a range or partners.