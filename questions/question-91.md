##### What WHO recommendations for countries?
***

WHO encourages all countries to enhance their surveillance for severe acute respiratory infections (SARI), to carefully review any unusual patterns of SARI or pneumonia cases and to notify WHO of any suspected or confirmed case of infection with novel coronavirus. 

Countries are encouraged to continue strengthening their preparedness for health emergencies in line with the International Health Regulations (2005).